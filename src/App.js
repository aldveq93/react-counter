import React, {useState, useEffect} from 'react';
import axios from 'axios';
import './App.css';

function App() {

  const [counter, setCounter] = useState(0);

  useEffect(() => {

    const callbackCounter = (response) => {
      setCounter(response.data[0].counter);
    }

    axios.get('http://localhost:3000/counter_data').then(callbackCounter);
  }, []);

  const get_counter_data = () => {
    const new_counter = counter + 1;
    const objRequest = {
      counter: new_counter
    }

    const callbackUpdateCounter = (response) => {
      setCounter(response.data.counter);
    }

    axios.put('http://localhost:3000/counter_data/1', objRequest).then(callbackUpdateCounter);
  }

  return (
    <div className="main-container">
      <button className="button" onClick={get_counter_data}>Get Counter Data</button>
      <div id="counter_data_container">
      <p>Contador: {counter}</p>
      </div>
    </div>
  );
}

export default App;
